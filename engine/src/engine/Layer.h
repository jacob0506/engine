#pragma once

#include"engine/core.h"
#include"engine/Events/Event.h"

namespace engine {
	class ENGINE_API Layer
	{
	public:
		Layer(const std::string& name = "layer");
		virtual ~Layer();

		virtual void OnAttach() {}
		virtual void OnDetach() {}
		virtual void OnUpdate() {}
		virtual void OnEvent(Event& event) {}

		inline const std::string& GetName() const { return m_DebugName; }
	protected:
		const std::string& m_DebugName;
	};
}