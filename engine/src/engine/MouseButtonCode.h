#pragma once

// From glfw3.h
#define AO_MOUSE_BUTTON_1         0
#define AO_MOUSE_BUTTON_2         1
#define AO_MOUSE_BUTTON_3         2
#define AO_MOUSE_BUTTON_4         3
#define AO_MOUSE_BUTTON_5         4
#define AO_MOUSE_BUTTON_6         5
#define AO_MOUSE_BUTTON_7         6
#define AO_MOUSE_BUTTON_8         7
#define AO_MOUSE_BUTTON_LAST      AO_MOUSE_BUTTON_8
#define AO_MOUSE_BUTTON_LEFT      AO_MOUSE_BUTTON_1
#define AO_MOUSE_BUTTON_RIGHT     AO_MOUSE_BUTTON_2
#define AO_MOUSE_BUTTON_MIDDLE    AO_MOUSE_BUTTON_3
