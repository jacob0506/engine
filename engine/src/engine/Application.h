#pragma once
#include"core.h"
#include"Windows.h"
#include"engine/LayerStack.h"
#include "engine/Events/Event.h"
#include "engine/Events/ApplicationEvent.h"


namespace engine {

	class ENGINE_API Application {
	public:
		Application();
		virtual ~Application();

		void Run();

		void OnEvent(Event& e);

		void PushLayer(Layer* layer);
		void PushOverlay(Layer* overlay);
		inline Window& GetWindow() { return *m_Window; }

		inline static Application& Get() { return *s_Instance; }
		
	private:
		bool OnWindowClosed(WindowCloseEvent& e);
		std::unique_ptr<Window> m_Window;
		bool m_Running = true;
		LayerStack m_LayerStack;

		static Application* s_Instance;
	};
	// To be define in client
	Application* createApplication();
}

