#pragma once

#ifdef EG_PLATFORM_WINDOWS

extern engine::Application* engine::createApplication();

	int main(int argc, char** argv) 
	{
		// printf_s("test");

		engine::Log::Init();
		EG_CORE_WARN("Initial Log");
		EG_INFO("hello");
		auto app = engine::createApplication();
		app->Run();
		delete app;
	}


#endif // EG_PLATFORM_WINDOWS
