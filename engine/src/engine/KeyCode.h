#pragma once
// From glfw3.h
#define AO_KEY_SPACE              32
#define AO_KEY_APOSTROPHE         39  /* ' */
#define AO_KEY_COMMA              44  /* , */
#define AO_KEY_MINUS              45  /* - */
#define AO_KEY_PERIOD             46  /* . */
#define AO_KEY_SLASH              47  /* / */
#define AO_KEY_0                  48
#define AO_KEY_1                  49
#define AO_KEY_2                  50
#define AO_KEY_3                  51
#define AO_KEY_4                  52
#define AO_KEY_5                  53
#define AO_KEY_6                  54
#define AO_KEY_7                  55
#define AO_KEY_8                  56
#define AO_KEY_9                  57
#define AO_KEY_SEMICOLON          59  /* ; */
#define AO_KEY_EQUAL              61  /* = */
#define AO_KEY_A                  65
#define AO_KEY_B                  66
#define AO_KEY_C                  67
#define AO_KEY_D                  68
#define AO_KEY_E                  69
#define AO_KEY_F                  70
#define AO_KEY_G                  71
#define AO_KEY_H                  72
#define AO_KEY_I                  73
#define AO_KEY_J                  74
#define AO_KEY_K                  75
#define AO_KEY_L                  76
#define AO_KEY_M                  77
#define AO_KEY_N                  78
#define AO_KEY_O                  79
#define AO_KEY_P                  80
#define AO_KEY_Q                  81
#define AO_KEY_R                  82
#define AO_KEY_S                  83
#define AO_KEY_T                  84
#define AO_KEY_U                  85
#define AO_KEY_V                  86
#define AO_KEY_W                  87
#define AO_KEY_X                  88
#define AO_KEY_Y                  89
#define AO_KEY_Z                  90
#define AO_KEY_LEFT_BRACKET       91  /* [ */
#define AO_KEY_BACKSLASH          92  /* \ */
#define AO_KEY_RIGHT_BRACKET      93  /* ] */
#define AO_KEY_GRAVE_ACCENT       96  /* ` */
#define AO_KEY_WORLD_1            161 /* non-US #1 */
#define AO_KEY_WORLD_2            162 /* non-US #2 */

/* Function keys */
#define AO_KEY_ESCAPE             256
#define AO_KEY_ENTER              257
#define AO_KEY_TAB                258
#define AO_KEY_BACKSPACE          259
#define AO_KEY_INSERT             260
#define AO_KEY_DELETE             261
#define AO_KEY_RIGHT              262
#define AO_KEY_LEFT               263
#define AO_KEY_DOWN               264
#define AO_KEY_UP                 265
#define AO_KEY_PAGE_UP            266
#define AO_KEY_PAGE_DOWN          267
#define AO_KEY_HOME               268
#define AO_KEY_END                269
#define AO_KEY_CAPS_LOCK          280
#define AO_KEY_SCROLL_LOCK        281
#define AO_KEY_NUM_LOCK           282
#define AO_KEY_PRINT_SCREEN       283
#define AO_KEY_PAUSE              284
#define AO_KEY_F1                 290
#define AO_KEY_F2                 291
#define AO_KEY_F3                 292
#define AO_KEY_F4                 293
#define AO_KEY_F5                 294
#define AO_KEY_F6                 295
#define AO_KEY_F7                 296
#define AO_KEY_F8                 297
#define AO_KEY_F9                 298
#define AO_KEY_F10                299
#define AO_KEY_F11                300
#define AO_KEY_F12                301
#define AO_KEY_F13                302
#define AO_KEY_F14                303
#define AO_KEY_F15                304
#define AO_KEY_F16                305
#define AO_KEY_F17                306
#define AO_KEY_F18                307
#define AO_KEY_F19                308
#define AO_KEY_F20                309
#define AO_KEY_F21                310
#define AO_KEY_F22                311
#define AO_KEY_F23                312
#define AO_KEY_F24                313
#define AO_KEY_F25                314
#define AO_KEY_KP_0               320
#define AO_KEY_KP_1               321
#define AO_KEY_KP_2               322
#define AO_KEY_KP_3               323
#define AO_KEY_KP_4               324
#define AO_KEY_KP_5               325
#define AO_KEY_KP_6               326
#define AO_KEY_KP_7               327
#define AO_KEY_KP_8               328
#define AO_KEY_KP_9               329
#define AO_KEY_KP_DECIMAL         330
#define AO_KEY_KP_DIVIDE          331
#define AO_KEY_KP_MULTIPLY        332
#define AO_KEY_KP_SUBTRACT        333
#define AO_KEY_KP_ADD             334
#define AO_KEY_KP_ENTER           335
#define AO_KEY_KP_EQUAL           336
#define AO_KEY_LEFT_SHIFT         340
#define AO_KEY_LEFT_CONTROL       341
#define AO_KEY_LEFT_ALT           342
#define AO_KEY_LEFT_SUPER         343
#define AO_KEY_RIGHT_SHIFT        344
#define AO_KEY_RIGHT_CONTROL      345
#define AO_KEY_RIGHT_ALT          346
#define AO_KEY_RIGHT_SUPER        347
#define AO_KEY_MENU               348