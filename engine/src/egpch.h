#pragma once

#include<iostream>
#include<string>
#include<sstream>
#include<memory>
#include<algorithm>
#include<utility>
#include<functional>
#include<vector>
#include<unordered_map>
#include<unordered_set>
#include<engine/Log.h>

#ifdef EG_PLATFORM_WINDOWS
	#include<Windows.h>
#endif // 

