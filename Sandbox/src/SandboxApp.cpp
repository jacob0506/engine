#include"Engine.h"
#include <glm/vec3.hpp> 
class ExampleLayer : public engine::Layer
{
public:
	ExampleLayer()
		:Layer("Example")
	{
		
	}

	void OnUpdate() override
	{
		//EG_CORE_INFO("Example::Update");
		if (engine::Input::IsKeyPressed(AO_KEY_TAB))
		{
			EG_TRACE("Tab key is pressed (poll)!");
		}

	}

	void OnEvent(engine::Event& event) override
	{
		//EG_CORE_TRACE("{0}", event);
		if (event.GetEventType() == engine::EventType::KeyPressed)
		{
			engine::KeyPressedEvent& e = (engine::KeyPressedEvent&)event;
			if (e.GetKeyCode() == AO_KEY_TAB)
			{
				EG_TRACE("Tab key pressed (event)!");
			}
			EG_TRACE("{0}", (char)e.GetKeyCode());
		}
	}
};

class Sandbox : public engine::Application
{
public:
	Sandbox()
	{
		PushLayer(new ExampleLayer());
		PushOverlay(new engine::ImGuiLayer());
	}
	~Sandbox()
	{

	}

private:

};

engine::Application* engine::createApplication() {
	return new Sandbox();
}