workspace "engine"
	architecture "x64"

	configurations
	{
		"Debug",
		"Release",
		"Dist"
	}

outputdir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"

IncludeDir = {}

IncludeDir["GLFW"] = "engine/vender/GLFW/include"
IncludeDir["Glad"] = "engine/vender/Glad/include"
IncludeDir["ImGui"] = "engine/vender/ImGui"
IncludeDir["glm"] = "engine/vender/glm"

include "engine/vender/GLFW"
include "engine/vender/Glad"
include "engine/vender/ImGui"

project "engine"
	 location "engine"
	 kind "SharedLib"
	 language "C++"

	 targetdir ("bin/" .. outputdir .. "/%{prj.name}")
	 objdir ("bin-int/" .. outputdir .. "/%{prj.name}")

	 pchheader "egpch.h"
	 pchsource "engine/src/egpch.cpp"

	 files
	 {
		  "%{prj.name}/src/**.h",
		  "%{prj.name}/src/**.cpp",
		  "%{prj.name}/vender/glm/glm/**.hpp",
		  "%{prj.name}/vender/glm/glm/**.inl"
	 }

	 includedirs
	 {
		"%{prj.name}/src",
		"%{prj.name}/vender/spdlog/include",
		"%{IncludeDir.GLFW}",
		"%{IncludeDir.Glad}",
		"%{IncludeDir.ImGui}",
		"%{IncludeDir.glm}"
	 }

	 links
	{
		"GLFW",
		"Glad",
		"ImGui",
        "opengl32.lib"
	}

	 filter "system:windows"
		cppdialect "C++17"
		staticruntime "On"
		systemversion "latest"

		defines
		{
			"EG_PLATFORM_WINDOWS",
			"EG_BUILD_DLL",
			"GLFW_INCLUDE_NONE"
		}

		postbuildcommands
		{
			("{COPY}" .. " %{wks.location}bin/Debug-windows-x86_64/engine/engine.dll" .. " %{wks.location}bin/Debug-windows-x86_64/Sandbox")
		}
	 
	filter "configurations:Debug"
		defines "EG_DEBUG"
		buildoptions "/MDd"
		symbols "On"

	filter "configurations:Release"
		defines "EG_RELEASE"
		buildoptions "/MD"
		optimize "On"
		
	filter "configurations:Dist"
		defines "EG_DIST"
		buildoptions "/MD"
		optimize "On"

project "Sandbox"
	location "Sandbox"
	kind "ConsoleApp"
	language "C++"

	targetdir ("bin/" .. outputdir .. "/%{prj.name}")
	objdir ("bin-int/" .. outputdir .. "/%{prj.name}")

	files
	{
		 "%{prj.name}/src/**.h",
		 "%{prj.name}/src/**.cpp"
	}

	includedirs
	{
		 "engine/vender/spdlog/include",
		 "engine/src",
		 "%{IncludeDir.glm}"
	}

	links
	{
		"engine"
	}

	filter "system:windows"
		cppdialect "C++17"
		staticruntime "On"
		systemversion "latest"

		defines
		{
			"EG_PLATFORM_WINDOWS"
		}

	filter "configurations:Debug"
		defines "EG_DEBUG"
		buildoptions "/MDd"
		symbols "On"

	filter "configurations:Release"
		defines "EG_RELEASE"
		buildoptions "/MD"
		optimize "On"
		
	filter "configurations:Dist"
		defines "EG_DIST"
		buildoptions "/MD"
		optimize "On"